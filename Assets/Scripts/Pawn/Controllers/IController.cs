using System;
using UniRx;
using UnityEngine;

public interface IController {
    /// <summary>
    /// IObservable z wektorem przemieszczenia si�
    /// uwzgl�dniaj�cy rotacj� postaci
    /// </summary>
    IObservable<Vector3> MoveVector { get; }

    IObservable<Unit> Jump { get; }
    ReadOnlyReactiveProperty<bool> Run { get; }
    ReadOnlyReactiveProperty<BodyPositions> BodyPosition { get; }
    IObservable<Unit> Take { get; }
    IObservable<Unit> Use { get; }
    IObservable<Unit> Inventory { get; }
    IObservable<Quaternion> XRotation { get; }
    IObservable<Quaternion> YRotation { get; }
}