using UniRx;
using UnityEngine;
using Zenject;

namespace Pawn {

    public sealed class HealthHandler : MonoBehaviour {
        private IPawn pawn;
        private NumericalStat health;

        //[Inject]
        //public void Construct(IPawn pawn) {
        //    this.pawn = pawn;
        //    this.health = pawn.Health;
        //}

        private void Awake() {
            pawn = GetComponent<IPawn>();
            health = pawn.Health;
        }

        private void Start() {
            pawn.DamageCommand
                .Subscribe(damageAmount => {
                    if (damageAmount < 0) { return; }

                    health.UnaryCurrent(-damageAmount);
                })
                .AddTo(this);
        }
    }
}