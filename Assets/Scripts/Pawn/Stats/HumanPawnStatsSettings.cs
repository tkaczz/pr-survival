using UnityEngine;
using System;

[Serializable]
public class HumanPawnStatsSettings {
    [Tooltip("Co jaki czas ma zwiększać. W sekundach.")]
    public float Interval = 1;

    public float MultiplerOnNormalState = 1;
    public float MultiplerOnRun = 3;

    [Header("Sleep")]
    [Tooltip("O ile ma zwiększać")]
    public float SleepAmountOnTick = 0.10f;
    [Tooltip("Ile ma dodawać wartości sleep przy skakaniu")]
    public float JumpSleepAmount = 4.0f;

    [Header("Sleep")]
    public float HungerAmountOnTick = 0.10f;
    public float HungerOnJump   = 4.0f;

    [Header("Thirst")]
    public float ThirstAmountOnTick = 0.1f;
    public float ThirstOnJump = 4.0f;
}