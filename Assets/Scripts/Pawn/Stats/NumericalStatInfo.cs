using System;

[Serializable]
public struct NumericalStatInfo {
    public float StartValue;
    public float MaxValue;
    public float MinValue;
}
