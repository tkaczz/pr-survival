using Global;
using Player;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;

namespace Pawn {

    public class RigidbodyCharacterController : MonoBehaviour {
        private FrameObservables frameObservables;
        private Rigidbody rigidBody;
        private CapsuleCollider characterCollider;
        private Transform playerTransform;

        [SerializeField]
        private float rayCastMargin = 0.1f;

        private float widthMargin;

        public ReadOnlyReactiveProperty<bool> IsOnGround { get; private set; }

        /// <summary>
        /// R�nica mi�dzy dwoma pozycjami postaci z poprzedniej i nast�pnej klatki
        /// Na sekund�, nie mi�dzy Time.fixedDeltaTime
        /// </summary>
        public ReadOnlyReactiveProperty<Vector3> Velocity { get; private set; }

        [Inject]
        private void Construct(
            FrameObservables frameObservables,
            Rigidbody rigidbody,
            [Inject(Id = "MainCollider")] CapsuleCollider characterCollider,
            [Inject(Id = Transforms.Pawn)] Transform playerTransform
        ) {
            this.frameObservables = frameObservables;
            this.rigidBody = rigidbody;
            this.characterCollider = characterCollider;
            this.playerTransform = playerTransform;
        }

        private void Awake() {
            InitVelocity();
            InitIsOnGround();
            InitDragCheck();
        }

        private void InitIsOnGround() {
            var maskExceptPlayer = ~(1 << LayerMask.NameToLayer("Player"));

            IsOnGround = Velocity
                .Where(velocity => velocity.sqrMagnitude > 0)
                .Select(_ => {
                    var spherePosition = new Vector3(
                            characterCollider.bounds.center.x,
                            characterCollider.bounds.center.y - (characterCollider.radius + rayCastMargin),
                            characterCollider.bounds.center.z
                        );
                    //Debug.Log(spherePosition);

                    var check = Physics.CheckSphere(
                            spherePosition,
                            characterCollider.radius,
                            maskExceptPlayer,
                            QueryTriggerInteraction.Ignore);
                    return check;
                })
                .ToReadOnlyReactiveProperty();

            //frameObservables.FixedUpdateAsObservable()
            //    .Where(_ => IsOnGround.Value == false)
            //    .Subscribe(_ => {
            //        Debug.Log("W powietrzu " + rigidBody.velocity);
            //    });
        }

        //tu jest kandydat to u�ycia Observable typu
        //pocz�tek jakiej� sekwencji
        //w trakcie
        //koniec sekwenci
        //poniewa� to ci�g�� sprawdzanie w where wygl�da brzydko
        private void InitDragCheck() {
            var inAirDrag = 0.02f;
            IsOnGround
                .Where(isOnGround => isOnGround == false)
                .Subscribe(_ => {
                    if (Mathf.Approximately(rigidBody.drag, inAirDrag) == false) {
                        rigidBody.drag = inAirDrag;
                    }
                })
                .AddTo(this);

            var onGroundDrag = 6;
            IsOnGround
                .Where(isOnGround => isOnGround == true)
                .Subscribe(_ => {
                    if (Mathf.Approximately(rigidBody.drag, onGroundDrag) == false) {
                        rigidBody.drag = onGroundDrag;
                    }
                })
                .AddTo(this);
        }

        private void InitVelocity() {
            var fixedTicksInSecond = 1 / Time.fixedDeltaTime;

            var previousPosition = frameObservables.FixedUpdateAsObservable()
                .Select(_ => {
                    return playerTransform.position;
                })
                .ToReadOnlyReactiveProperty<Vector3>()
                .AddTo(this);

            Velocity = frameObservables.FixedUpdateAsObservable()
                .SampleFrame(1)
                .Select(_ => {
                    var nextPosition = playerTransform.position;
                    var velocity = nextPosition - previousPosition.Value;
                    return velocity * fixedTicksInSecond;
                })
                .ToReadOnlyReactiveProperty<Vector3>()
                .AddTo(this);
        }

        //todo
        //wg mnie lepiej by by�o jakby to ten kontroler decydowa� czy funkcja ma si� odpala� w fixed update czy nie
        public void MoveForce(Vector3 motion) {
            rigidBody.MovePosition(transform.position + motion);
        }

        public void MoveImpulse(Vector3 motion) {
            //Debug.Log("Skakanie " + motion);
            rigidBody.AddForce(motion, ForceMode.Impulse);
        }
    }
}