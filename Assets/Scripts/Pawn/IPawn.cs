using UniRx;
using UnityEngine;
using Zenject;

public interface IPawn {
    NumericalStat Health { get; }
    ReadOnlyReactiveProperty<bool> IsDead { get; }
    Subject<float> DamageCommand { get; }
}