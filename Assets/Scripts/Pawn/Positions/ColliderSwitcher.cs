using UniRx;
using UnityEngine;
using Zenject;

public sealed class ColliderSwitcher : MonoBehaviour {
    private new CapsuleCollider collider;
    private PositionController positionController;
    private new Rigidbody rigidbody;

    private float baseHeight;

    [Inject]
    public void Construct(
        PositionController positionController,
        [Inject(Id="MainCollider")]
        CapsuleCollider collider,
        Rigidbody rigidbody
    ) {
        this.positionController = positionController;
        this.collider           = collider;
        this.rigidbody          = rigidbody;
    }

    private void Awake() {
        baseHeight = collider.height;

        positionController.CurrentHeightMultipler
            .Subscribe(nextHeightMultipler => {
                rigidbody.useGravity = false;

                collider.height = baseHeight * nextHeightMultipler;

                var currentCenterHeight = -1 + nextHeightMultipler;
                collider.center = new Vector3(0, currentCenterHeight,0);

                rigidbody.useGravity = true;
            })
            .AddTo(this);
    }
}