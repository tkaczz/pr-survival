using UniRx;
using UnityEngine;

public class PositionController : MonoBehaviour {
    public ReactiveProperty<float> CurrentHeightMultipler { get; set; }

    private void Awake() {
        CurrentHeightMultipler = new ReactiveProperty<float>(1.0f);
    }
}