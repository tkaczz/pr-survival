using UnityEngine;
using UniRx;
using Zenject;
using Pawn;

namespace Pawn {

    public sealed class CrouchPosition : MonoBehaviour {
        private IController pawnController;
        private IMovementController movementController;
        private RigidbodyCharacterController characterController;
        private PositionController positionController;
        private float heightMultipler = 0.5f;
        private float speedMultipler = 0.5f;

        [Inject]
        public void Construct(
            IController pawnController,
            IMovementController movementController,
            RigidbodyCharacterController characterController,
            PositionController positionController
        ) {
            this.pawnController = pawnController;
            this.movementController = movementController;
            this.characterController = characterController;
            this.positionController = positionController;
        }

        private void Awake() {
            pawnController.BodyPosition
                .Where(position => {
                    var check =
                        position == BodyPositions.Crouch &&
                        characterController.IsOnGround.Value == true;
                    return check;
                })
                .Subscribe(position => {
                    //Debug.Log("Crouching");
                    movementController.SpeedMultipler = speedMultipler;
                    positionController.CurrentHeightMultipler.Value = heightMultipler;
                })
                .AddTo(this);
        }
    }
}