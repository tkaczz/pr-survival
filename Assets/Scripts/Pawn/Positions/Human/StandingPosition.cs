using UnityEngine;
using UniRx;
using Zenject;

namespace Pawn {

    public sealed class StandingPosition : MonoBehaviour {
        private IController pawnController;
        private IMovementController movementController;
        private RigidbodyCharacterController characterController;
        private PositionController positionController;
        private float heightMultipler = 1.0f;
        private float speedMultipler = 1.0f;

        [Inject]
        public void Construct(
            IController pawnController,
            IMovementController movementController,
            RigidbodyCharacterController characterController,
            PositionController positionController
        ) {
            this.pawnController = pawnController;
            this.movementController = movementController;
            this.characterController = characterController;
            this.positionController = positionController;
        }

        private void Awake() {
            pawnController.BodyPosition
                .Where(position => {
                    var check =
                        position == BodyPositions.Standing &&
                        characterController.IsOnGround.Value == true;
                    return check;
                })
                .Subscribe(position => {
                    //Debug.Log("Standing");
                    movementController.SpeedMultipler = speedMultipler;
                    positionController.CurrentHeightMultipler.Value = heightMultipler;
                })
                .AddTo(this);
        }
    }
}