﻿using UniRx;
using System;

[Serializable]
public struct PawnInformation {
	public Gender PawnGender;
	public IntReactiveProperty Age;
	public FloatReactiveProperty Weight;
	public FloatReactiveProperty Height;
}