﻿using System;
using Global;
using Player;
using UniRx;
using UnityEngine;
using Zenject;

public class GameEnd : MonoBehaviour {
    private GameMode gameMode;
    private PlayerPawn player;

    public IObservable<Unit> EndOfGameEvent { get; private set; }

    private void Awake() {
        EndOfGameEvent = player.IsDead
            .Select(_ => {
                return new Unit();
            });

        EndOfGameEvent
            .Subscribe(_ => {
                gameMode.EndGame();
            })
            .AddTo(this);

        //obsłużenie końca gry
        gameMode.WorldState
            .Where(state => state == WorldStates.Ending)
            .Subscribe(_ => {
                Time.timeScale = 0;
            })
            .AddTo(this);
    }

    [Inject]
    public void Construct(
        GameMode gameMode,
        PlayerPawn player
    ) {
        this.gameMode = gameMode;
        this.player = player;
    }
}