using UnityEngine;
using Zenject;

namespace Global {
    public class WorldInit : MonoBehaviour {
        private GameMode gameMode;

        [Inject]
        public void Construct(GameMode gameMode) {
            this.gameMode = gameMode;
        }

        private void Awake() {
            gameMode.CreateWorld();
        }
    }
}