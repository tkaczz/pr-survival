﻿using System;
using System.Windows.Input;
using Zenject;

public class SwitchToInventoryViewCommand : ICommand {
    private MainViewPresenter mainViewPresenter;

    [Inject]
    public void Construct(MainViewPresenter mainViewPresenter) {
        this.mainViewPresenter = mainViewPresenter;
    }

#pragma warning disable 0067
    public event EventHandler CanExecuteChanged;
#pragma warning restore 0067

    public bool CanExecute(object parameter) {
        //tak jak przy innych rozkazach
        return true;
    }

    public void Execute(object parameter) {
        mainViewPresenter.SetMainViewState(MainViewStates.Inventory);
    }
}