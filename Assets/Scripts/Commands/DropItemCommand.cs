﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using UnityEngine;
using Zenject;

/// <summary>
/// Za parametr przyjmuje ilość przedmiotów
/// </summary>
public class DropItemCommand : ICommand {
    private InventoryPresenter inventoryPresenter;
    private Inventory playersInventory;

#pragma warning disable 0067

    public event EventHandler CanExecuteChanged;

#pragma warning restore 0067

    [Inject]
    public void Construct(
        InventoryPresenter inventoryPresenter,
        Inventory playersInventory
    ) {
        this.inventoryPresenter = inventoryPresenter;
        this.playersInventory = playersInventory;
    }

    public bool CanExecute(object parameter) {
        //odświeżanie w noesis i tak nie działa więc zostawiam tak jak jest
        return true;
    }

    public void Execute(object parameter) {
        var count = inventoryPresenter.ItemsListBox.SelectedItems.Count;
        if (count == 0) { return; }

        //trzeba pobrać listę przedmiotów, nie zaznaczonych i potem po kolei dropić
        Dictionary<ItemTypes, int> itemsToDrop = new Dictionary<ItemTypes, int>();
        foreach (ItemEntry item in inventoryPresenter.ItemsListBox.SelectedItems) {
            itemsToDrop.Add(item.ItemType, (int)parameter);
        }

        foreach (var itemToDrop in itemsToDrop) {
            //Debug.Log("Drop " + itemToDrop.Key + " " + itemToDrop.Value);
            playersInventory.DropItem(itemToDrop.Key, itemToDrop.Value);
        }

        //przekazujemy że obiekt jest przeznaczony do sprzątnięcia
        itemsToDrop = null;
    }
}