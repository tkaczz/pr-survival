﻿using System.Windows.Input;
using Noesis;
using Zenject;

public class CraftCommand : ICommand {
    private CraftingSystem craftingSystem;
    private CraftingPresenter craftingPresenter;

#pragma warning disable 0067

    public event System.EventHandler CanExecuteChanged;

#pragma warning restore 0067

    [Inject]
    public void Construct(
        CraftingSystem craftingSystem,
        CraftingPresenter craftingPresenter
    ) {
        this.craftingSystem = craftingSystem;
        this.craftingPresenter = craftingPresenter;
    }

    public bool CanExecute(object parameter) {
        //jako że odświeżanie nie dziala to ustawiam że zawsze zwróci true
        return true;
    }

    public void Execute(object parameter) {
        //Debug.Log("Craft command");
        var listBox = (ListBox)parameter;

        foreach (RecipeEntry recipeProduct in listBox.SelectedItems) {
            craftingSystem.CraftItems(recipeProduct.OriginRecipe);
        }

        //po wszystkim warto odświeżyć listę
        craftingPresenter.RefreshRecipesList();
    }
}