﻿using System.Windows.Input;
using Noesis;

/// <summary>
/// Za parametr przyjmuje ListBox/ListView
/// </summary>
public class DeselectAllCommand : ICommand {
#pragma warning disable 0067

    public event System.EventHandler CanExecuteChanged;

#pragma warning restore 0067

    public bool CanExecute(object parameter) {
        //tymczasowo
        return true;
    }

    public void Execute(object parameter) {
        var listbox = (ListBox)parameter;

        if (listbox.SelectionMode != SelectionMode.Single) {
            listbox.SelectedItems.Clear();
        }
        else {
            listbox.SelectedItem = null;
        }
    }
}