﻿using System;
using System.Windows.Input;
using UnityEngine;

public class ExitGameCommand : ICommand {

    public event EventHandler CanExecuteChanged;

    public bool CanExecute(object parameter) {
        return true;
    }

    public void Execute(object parameter) {
        Application.Quit();
    }
}