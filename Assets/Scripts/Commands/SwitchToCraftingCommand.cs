﻿using System.Collections.Generic;
using System.Windows.Input;
using Noesis;
using Zenject;

/// <summary>
/// Parametr Execute - ListBox/ListView z ekwipunku z zaznaczonymi przedmiotami
/// </summary>
public class SwitchToCraftingCommand : ICommand {
    private MainViewPresenter mainViewPresenter;
    private CraftingPresenter craftingPresenter;
    private RecipesDatabase recipesDatabase;
    private Inventory inventory;

#pragma warning disable 0067

    public event System.EventHandler CanExecuteChanged;

#pragma warning restore 0067

    [Inject]
    public void Construct(
        MainViewPresenter mainViewPresenter,
        CraftingPresenter craftingPresenter,
        RecipesDatabase recipesDatabase,
        Inventory inventory
    ) {
        this.mainViewPresenter = mainViewPresenter;
        this.craftingPresenter = craftingPresenter;
        this.recipesDatabase = recipesDatabase;
        this.inventory = inventory;
    }

    public bool CanExecute(object parameter) {
        //to i tak nie działa więc ustawiam że zawsze jest true
        return true;
    }

    public void Execute(object parameter) {
        var listBox = (ListBox)parameter;
        var selectedItems = listBox.SelectedItems;

        if (listBox.SelectedItems.Count == 0) {
            //Debug.Log("Nic nie zaznaczone");
            return;
        }

        var ingridients = new Dictionary<ItemTypes, int>();

        //nie powinienem używać obiektu przeznaczonego do wyświetlania
        //do wykonywania logiki, później się za to wezmę
        for (int i = 0; i < selectedItems.Count; i++) {
            ItemEntry current = (ItemEntry)selectedItems[i];

            ingridients.Add(current.ItemType, current.Quantity);
        }

        var check = recipesDatabase.CheckIfSomethingCanBeCreated(ingridients);
        if (check == false) {
            //Debug.Log("Nie znaleziono receptur");
            return;
        }
        else {
            //Debug.Log("Znalazłem receptury");
            //można ewentualnie inaczej i mało inwazyjnie
            //przekazać po ref listę, i drugi parametr tablicę/listę co ma wyciągnąć
            craftingPresenter.CreateEntries(ingridients);
            mainViewPresenter.SetMainViewState(MainViewStates.Crafting);
        }
    }
}