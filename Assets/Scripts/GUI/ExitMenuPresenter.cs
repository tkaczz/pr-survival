﻿using UniRx;
using UnityEngine;
using Zenject;
using Player;

public class ExitMenuPresenter : MonoBehaviour {
    private KeyboardMouseInputHandler input;

    public ExitGameCommand ExitGameCommand { get; private set; }
    public ReadOnlyReactiveProperty<bool> IsVisible;

    [Inject]
    public void Construct(
        KeyboardMouseInputHandler input,
        ExitGameCommand exitGameCommand

    ) {
        this.input = input;
        this.ExitGameCommand = exitGameCommand;
    }

    private void Start() {
        InitIsVisible();
        InitHideShow();
    }

    private void InitIsVisible() {
        IsVisible = input.ActionInputs
            .Where(actionInputs => actionInputs.ExitMenu == true)
            .Select(_ => {
                return !IsVisible.Value;
            })
            .ToReadOnlyReactiveProperty()
            .AddTo(this);
    }

    private void InitHideShow() {
        var nv = GetComponent<NoesisView>();
        var content = nv.Content;

        var main = (Noesis.Viewbox)content.FindName("MainUI");
        var actions = (Noesis.Grid)content.FindName("Actions");

        actions.DataContext = this;

        IsVisible
            .Where(state => state == true)
            .Subscribe(_ => {
                main.Visibility = Noesis.Visibility.Visible;
            })
            .AddTo(this);

        IsVisible
            .Where(state => state == false)
            .Subscribe(_ => {
                main.Visibility = Noesis.Visibility.Hidden;
            })
            .AddTo(this);
    }
}