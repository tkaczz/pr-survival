﻿using Noesis;
using Player;
using System.Collections.ObjectModel;
using UniRx;
using UnityEngine;
using Zenject;

public class MainViewPresenter : MonoBehaviour {
    private PlayerController playerController;

    private FrameworkElement root;
    public ContentControl MainContent { get; private set; }

    private ReactiveProperty<bool> isVisible;
    public ReadOnlyReactiveProperty<bool> IsVisible { get; private set; }

    private ReactiveProperty<MainViewStates> currentMainViewState;
    public ReadOnlyReactiveProperty<MainViewStates> CurrentMainViewState { get; private set; }

    public MainViewStates DefaultMainViewState { get; private set; }

    [Inject]
    public void Construct(PlayerController playerController) {
        this.playerController = playerController;
    }

    private void Start() {
        SetMainContent();
        InitCurrentState();
        InitIsVisible();
        InitViewToggle();
    }

    private void SetMainContent() {
        root = GetComponent<NoesisView>().Content;
        MainContent = (ContentControl)root.FindName("MainContent");
    }

    private void InitCurrentState() {
        currentMainViewState = new ReactiveProperty<MainViewStates>();
        CurrentMainViewState = new ReadOnlyReactiveProperty<MainViewStates>(currentMainViewState);
    }

    private void InitIsVisible() {
        isVisible = new ReactiveProperty<bool>(false);
        IsVisible = new ReadOnlyReactiveProperty<bool>(isVisible);

        playerController.Inventory
            .Subscribe(_ => {
                isVisible.Value = !isVisible.Value;
            })
            .AddTo(this);
    }

    private void InitViewToggle() {
        isVisible
            .Where(visibiltyState => visibiltyState == true)
            .Subscribe(visibilityState => {
                //wywołanie domyślnego widoku, jeśli wywołaliśmy ekran
                currentMainViewState.Value = DefaultMainViewState;

                //wyświetlenie
                root.Visibility = Visibility.Visible;
            })
            .AddTo(this);

        isVisible
            .Where(visibilityState => visibilityState == false)
            .Subscribe(visibilityState => {
                root.Visibility = Visibility.Hidden;
            })
            .AddTo(this);
    }

    public void ChangeDeaultMainViewState(MainViewStates mainViewState) {
        DefaultMainViewState = mainViewState;
    }

    public void SetMainViewState(MainViewStates mainViewState) {
        currentMainViewState.Value = mainViewState;
    }
}

public static class ObservableCollectionExtensions {
    public static ItemEntry FindItem(this ObservableCollection<ItemEntry> col, ItemTypes itemType) {
        var count = col.Count;

        for (int i = 0; i < count; i++) {
            var current = col[i];
            if (current.ItemType == itemType) {
                return col[i];
            }
        }

        //not found
        return null;
    }
}

public enum MainViewStates {
    None,
    Inventory,
    Crafting
}