﻿using Noesis;

public class ItemEntry {
    public ItemTypes ItemType { get; private set; }
    public string Name { get; private set; }
    public TextureSource Thumbnail { get; private set; }
    public int Quantity { get; private set; }
    public string Description { get; private set; }

    public ItemEntry(
        ItemTypes itemType,
        TextureSource itemTexture,
        int quantity,
        string description
    ) {
        ItemType = itemType;
        Name = itemType.ToString();
        Thumbnail = itemTexture;
        Quantity = quantity;
        Description = description;
    }
}