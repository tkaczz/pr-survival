﻿using Player;
using System;
using UnityEngine;
using Zenject;
using Noesis;
using UniRx;

public class StatsHUD_Presenter : MonoBehaviour {
    private PlayerPawn playerPawn;
    private Inventory playerInventory;

    private FrameworkElement root;
    private ProgressBar healthBar;

    private TextBlock currentWeight;
    private TextBlock maxWeight;

    [Inject]
    public void Construct(
        PlayerPawn playerPawn,
        Inventory playerInventory
    ) {
        this.playerPawn = playerPawn;
        this.playerInventory = playerInventory;
    }

    private void Start() {
        root = GetComponent<NoesisView>().Content;
        healthBar = (ProgressBar)root.FindName("HealthBar");

        currentWeight = (TextBlock)root.FindName("CurrentWeight");
        maxWeight = (TextBlock)root.FindName("MaxWeight");

        InitSubscribes();
        InitWeight();
    }

    private void InitSubscribes() {
        SetProgressBarSubscribes(playerPawn.Health, healthBar);
    }

    private void InitWeight() {
        currentWeight.Text = playerInventory.Weight.Current.Value.ToString();
        maxWeight.Text = playerInventory.Weight.Max.Value.ToString();

        playerInventory.Weight.Current
            .Subscribe(newWeight => {
                currentWeight.Text = newWeight.ToString();
            })
            .AddTo(this);

        playerInventory.Weight.Max
            .Subscribe(newMaxWeight => {
                maxWeight.Text = newMaxWeight.ToString();
            })
            .AddTo(this);
    }

    private void SetProgressBarSubscribes(NumericalStat numStat, ProgressBar progBar) {
        numStat.Min
            .Subscribe(newMin => {
                progBar.Minimum = newMin;
            })
            .AddTo(this);

        numStat.Current
            .Subscribe(newCurrent => {
                progBar.Value = newCurrent;
            })
            .AddTo(this);

        numStat.Max
            .Subscribe(newMax => {
                progBar.Maximum = newMax;
            })
            .AddTo(this);
    }
}