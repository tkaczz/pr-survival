﻿using Global;
using Noesis;
using UniRx;
using UnityEngine;
using Zenject;

public class GameEndPresenter : MonoBehaviour {
    public ExitGameCommand ExitGameCommand { get; private set; }

    private Viewbox mainContent;
    private GameMode gameMode;

    [Inject]
    public void Construct(
        ExitGameCommand exitGameCommand,
        GameMode gameMode
    ) {
        this.ExitGameCommand = exitGameCommand;
        this.gameMode = gameMode;
    }

    private void Start() {
        gameMode.WorldState
            .Where(state => state == WorldStates.Ending)
            .Subscribe(_ => {
                mainContent.Visibility = Visibility.Visible;
            })
            .AddTo(this);

        var nv = GetComponent<NoesisView>();
        var content = nv.Content;
        mainContent = (Viewbox)content.FindName("MainContent");
        mainContent.DataContext = this;
    }
}