﻿using Noesis;
using UnityEngine;
using Zenject;
using UniRx;

public class PickupLogoPresenter : MonoBehaviour {
    private PickupHandler pickupHandler;
    private Storyboard fadeIn;
    private Storyboard fadeOut;

    [Inject]
    public void Construct(PickupHandler pickupHandler) {
        this.pickupHandler = pickupHandler;
    }

    private void Start() {
        var root = GetComponent<NoesisView>().Content;
        fadeIn = (Storyboard)root.FindResource("FadeIn");
        fadeOut = (Storyboard)root.FindResource("FadeOut");

        pickupHandler.ObjectInRange
            .Where(objectMessage => objectMessage.Object != null)
            .Subscribe(itemObject => {
                fadeIn.Begin();
            })
            .AddTo(this);

        pickupHandler.ObjectInRange
            .Where(objectMessage => objectMessage.Object == null)
            .Subscribe(itemObject => {
                fadeOut.Begin();
            })
            .AddTo(this);
    }
}