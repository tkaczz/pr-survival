﻿using Noesis;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UniRx;
using UnityEngine;
using Zenject;

public class CraftingPresenter : MonoBehaviour {

    #region gui var's

    private Page craftingView;

    #endregion gui var's

    private MainViewPresenter mainViewPresenter; //musi byc
    private InventoryPresenter inventoryPresenter; //raczej tez
    private ItemEntriesFactory itemEntriesFactory; //raczej tez
    private RecipesDatabase recipesDatabase; //raczej tez
    private Inventory inventory;

    private IDictionary<ItemTypes, int> currentIngridientsContext;

    public ObservableCollection<ItemEntry> IngridientsContext { get; private set; }
    public ObservableCollection<RecipeEntry> CurrentAvailableProducts { get; private set; }

    public SwitchToInventoryViewCommand BackToItems { get; private set; }
    public DeselectAllCommand DeselectAllCommand { get; private set; }
    public CraftCommand CraftCommand { get; private set; }

    [Inject]
    public void Construct(
        MainViewPresenter mainViewPresenter,
        InventoryPresenter inventoryPresenter,
        ItemEntriesFactory itemEntriesFactory,
        RecipesDatabase recipesDatabase,
        Inventory inventory,
        SwitchToInventoryViewCommand switchToInventoryViewCommand,
        DeselectAllCommand deselectAllCommand,
        CraftCommand craftCommand
    ) {
        this.mainViewPresenter = mainViewPresenter;
        this.inventoryPresenter = inventoryPresenter;
        this.itemEntriesFactory = itemEntriesFactory;
        this.recipesDatabase = recipesDatabase;
        this.inventory = inventory;
        this.BackToItems = switchToInventoryViewCommand;
        this.DeselectAllCommand = deselectAllCommand;
        this.CraftCommand = craftCommand;
    }

    private void Start() {
        InitView();
        InitObservables();
        //dojdzie jeszcze podłączenie do zdarzenia, że stworzono przedmiot z receptury
        SetUI();
    }

    private void InitView() {
        craftingView = (Page)Noesis.GUI.LoadXaml("Assets/Scripts/Views/CraftingView.xaml");

        mainViewPresenter.CurrentMainViewState
            .Where(state => state == MainViewStates.Crafting)
            .Subscribe(_ => {
                mainViewPresenter.MainContent.Content = craftingView;
            })
            .AddTo(this);
    }

    private void InitObservables() {
        IngridientsContext = new ObservableCollection<ItemEntry>();
        CurrentAvailableProducts = new ObservableCollection<RecipeEntry>();
    }

    private void SetUI() {
        var currentIngridients = (ListBox)craftingView.FindName("CurrentIngridients");
        currentIngridients.DataContext = this;

        var availableProducts = (ListBox)craftingView.FindName("AvailableProducts");
        availableProducts.DataContext = this;

        var actions = (UniformGrid)craftingView.FindName("Actions");
        actions.DataContext = this;
    }

    private void CreateIngridientsEntries(IDictionary<ItemTypes, int> ingridients) {
        IngridientsContext.Clear();

        //trzeba pobrać referencje
        foreach (var ingridient in ingridients) {
            var currentPick = inventoryPresenter.FindItemEntry(ingridient.Key);
            if (currentPick != null) {
                IngridientsContext.Add(currentPick);
            }
            //może zrobić w ten sposób
            //jeżeli nie ma to usuwamy składnik z kontekstu
            //potem CreateRecipeEntries wykorzystuje ten kontekst
            //-> mniejsza alokacja obiektów
            else { return; }
        }
    }

    /// <summary>
    /// Tworzy NOWĄ listę receptur gotowych do wyświetlenia w UI
    /// </summary>
    /// <param name="ingridientsCollection">Wybrane przez użytkownika składniki</param>
    public void CreateEntries(IDictionary<ItemTypes, int> ingridients) {
        currentIngridientsContext = ingridients;

        CreateIngridientsEntries(currentIngridientsContext);
        CreateRecipeEntries(currentIngridientsContext);

        //trochę tego śmiecia się nazbiera więc lepiej chyba odświeżyć
        System.GC.Collect();
    }

    private void CreateRecipeEntries(IDictionary<ItemTypes, int> ingridients) {
        CurrentAvailableProducts.Clear();

        Dictionary<ItemTypes, int> existsIngridients = new Dictionary<ItemTypes, int>();
        foreach (var ingridient in ingridients) {
            if (inventory.Items.ContainsKey(ingridient.Key)) {
                existsIngridients.Add(ingridient.Key, ingridient.Value);
            }
            else { return; }
        }

        var availableRecipes = recipesDatabase.FindMatchingRecipes(existsIngridients);
        if (availableRecipes == null || availableRecipes.Count == 0) {
            return;
        }

        foreach (var recipe in availableRecipes) {
            var newProductsList = new RecipeEntry();

            newProductsList.OriginRecipe = recipe;

            foreach (var ingridient in recipe.Ingridients) {
                var currentIngridient = itemEntriesFactory.CreateItemEntry(
                    ingridient.Key,
                    ingridient.Value
                );

                newProductsList.Ingridients.Add(currentIngridient);
            }

            foreach (var product in recipe.RecipeProducts) {
                var currentProduct = itemEntriesFactory.CreateItemEntry(
                    product.Key,
                    product.Value
                );

                newProductsList.Products.Add(currentProduct);
            }

            CurrentAvailableProducts.Add(newProductsList);
        }
    }

    /// <summary>
    /// Odświeża listę receptur na bazie składników przekazanych w CreateRecipesList
    /// </summary>
    public void RefreshRecipesList() {
        CreateIngridientsEntries(currentIngridientsContext);
        CreateRecipeEntries(currentIngridientsContext);
    }
}

public class RecipeEntry {
    public RecipesDatabase.Recipe OriginRecipe { get; set; }
    public ObservableCollection<ItemEntry> Ingridients { get; set; }
    public ObservableCollection<ItemEntry> Products { get; set; }

    public RecipeEntry() {
        Ingridients = new ObservableCollection<ItemEntry>();
        Products = new ObservableCollection<ItemEntry>();
    }
}