﻿using Noesis;
using System.Collections.ObjectModel;
using UniRx;
using UnityEngine;
using Zenject;

public class InventoryPresenter : MonoBehaviour {

    #region gui var's

    private Inventory playersInventory;
    private ItemEntriesFactory itemEntriesFactory;
    private MainViewPresenter mainViewPresenter;

    private Page inventoryView;
    private UniformGrid actions;

    private StackPanel itemDetails;

    public ListBox ItemsListBox { get; private set; }

    #endregion gui var's

    public ObservableCollection<ItemEntry> ItemsEntriesList { get; private set; }

    public DropItemCommand DropItemCommand { get; private set; }
    public DeselectAllCommand DeselectAllCommand { get; private set; }
    public SwitchToCraftingCommand SwitchToCraftingCommand { get; private set; }

    [Inject]
    public void Construct(
        Inventory playersInventory,
        ItemEntriesFactory itemEntriesFactory,
        MainViewPresenter mainViewPresenter,
        DropItemCommand dropItemCommand,
        DeselectAllCommand deselectAllCommand,
        SwitchToCraftingCommand switchToCraftingCommand
    ) {
        this.playersInventory = playersInventory;
        this.itemEntriesFactory = itemEntriesFactory;
        this.mainViewPresenter = mainViewPresenter;
        this.DropItemCommand = dropItemCommand;
        this.DeselectAllCommand = deselectAllCommand;
        this.SwitchToCraftingCommand = switchToCraftingCommand;
    }

    private void Start() {
        InitInventoryView();
        InitItemsList();
        SetSelfAsDeault();
        InitItemDetails();
    }

    private void InitItemsList() {
        ItemsEntriesList = new ObservableCollection<ItemEntry>();

        //dodawanie nowego przedmiotu
        playersInventory.Items
            .ObserveAdd()
            .Subscribe(newItem => {
                ItemsEntriesList.Add(
                    itemEntriesFactory.CreateItemEntry(newItem.Key, newItem.Value)
                );
            })
            .AddTo(this);

        //usuwanie przedmiotu
        playersInventory.Items
            .ObserveRemove()
            .Subscribe(removedItem => {
                var itemToRemove = ItemsEntriesList.FindItem(removedItem.Key);
                ItemsEntriesList.Remove(itemToRemove);
            })
            .AddTo(this);

        //zmiana ilości
        playersInventory.Items
            .ObserveReplace()
            .Subscribe(changedItem => {
                var index = ItemsEntriesList.IndexOf(ItemsEntriesList.FindItem(changedItem.Key));

                ItemsEntriesList[index] = itemEntriesFactory.CreateItemEntry(
                    changedItem.Key, changedItem.NewValue
                );
            })
            .AddTo(this);
    }

    private void SetSelfAsDeault() {
        mainViewPresenter.ChangeDeaultMainViewState(MainViewStates.Inventory);

        mainViewPresenter.CurrentMainViewState
            .Where(state => state == MainViewStates.Inventory)
            .Subscribe(_ => {
                mainViewPresenter.MainContent.Content = inventoryView;
            })
            .AddTo(this);
    }

    private void InitInventoryView() {
        inventoryView = (Page)Noesis.GUI.LoadXaml("Assets/Scripts/Views/InventoryView.xaml");

        ItemsListBox = (ListBox)inventoryView.FindName("ItemsListBox");
        ItemsListBox.DataContext = this;

        actions = (UniformGrid)inventoryView.FindName("Actions");
        actions.DataContext = this;
    }

    private void InitItemDetails() {
        itemDetails = (StackPanel)inventoryView.FindName("ItemDetails");
        ItemsListBox.SelectionChanged += (sender, e) => {
            itemDetails.DataContext = (ItemEntry)ItemsListBox.SelectedItem;
        };
    }

    public ItemEntry FindItemEntry(ItemTypes itemType) {
        var count = ItemsEntriesList.Count;
        for (int i = 0; i < count; i++) {
            if (ItemsEntriesList[i].ItemType == itemType) {
                return ItemsEntriesList[i];
            }
        }

        return null;
    }
}