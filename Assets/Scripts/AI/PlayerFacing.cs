﻿using Panda;
using Player;
using System;
using UnityEngine;
using Zenject;

public class PlayerFacing : MonoBehaviour {
    [SerializeField]
    private Settings settings;

    private Transform aiPawnTransform;
    private Transform playerTransform;
    private bool rotating;

    [Inject]
    public void Construct(PlayerPawn player) {
        this.playerTransform = player.GetComponent<Transform>();
        aiPawnTransform = GetComponent<Transform>();
    }

    [Task]
    private void LookAtPlayer() {
        rotating = true;
        Task.current.Succeed();
    }

    [Task]
    private void DontLookAtPlayer() {
        rotating = false;
        Task.current.Succeed();
    }

    private void Update() {
        if (rotating) {
            var lookDirection = (playerTransform.position - aiPawnTransform.position).normalized;

            //create the rotation we need to be in to look at the target
            var lookRotation = Quaternion.LookRotation(lookDirection);

            //rotate us over time according to speed until we are in the required rotation
            transform.rotation = Quaternion.Slerp(
                transform.rotation,
                lookRotation,
                Time.deltaTime * settings.RotationSpeed
            );
        }
    }

    [Serializable]
    private class Settings {
        public float playerDetectionDistance = 5f;
        public float RotationSpeed = 5;
    }
}