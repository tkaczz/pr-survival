﻿using Panda;
using Player;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

/// <summary>
/// Odpowiedzialne za poruszanie się z użyciem agenta AI
/// </summary>
public class TigerNavigation : MonoBehaviour {
    private WaypointPathfinder waypointPathfinder;
    private TigerController controller;
    private PlayerPawn playerPawn;
    private Transform playerTransform;

    public Transform animalTransform { get; private set; }
    private NavMeshAgent navAgent;

    [Task]
    public bool movingFlag { get; private set; }

    [Task]
    public bool inProgressFlag { get; private set; }

    /// <summary>
    /// Can be null in two cases
    /// <para>On start when it's set to null</para>
    /// <para>If entered into combat mode</para>
    /// </summary>
    private WaypointNode currentWayPoint;

    [Inject]
    public void Construct(
        WaypointPathfinder waypointPathfinder,
        PlayerPawn playerPawn
    ) {
        this.waypointPathfinder = waypointPathfinder;
        this.controller = GetComponent<TigerController>();
        this.playerPawn = playerPawn;
        this.playerTransform = playerPawn.GetComponent<Transform>();
    }

    private void Awake() {
        animalTransform = GetComponent<Transform>();
        navAgent = GetComponent<NavMeshAgent>();

        SetMoveFlags(false);
    }

    private void Update() {
        if (CheckMoveFlags() == false) { return; }
        if (navAgent.remainingDistance > navAgent.stoppingDistance) {
            //Debug.Log("Moving");
            controller.MoveCommand(navAgent.desiredVelocity);
        }
        else {
            //Debug.Log("Stopping");
            StopMoving();
        }
    }

    [Task]
    private void RunTowardsPlayer() {
        MoveTo(playerPawn.transform.position, true);
        Task.current.Succeed();
    }

    private bool CheckMoveFlags() {
        return movingFlag && inProgressFlag;
    }

    private void SetMoveFlags(bool state) {
        movingFlag = state;
        inProgressFlag = state;
    }

    private void MoveTo(Vector3 position, bool run = false) {
        controller.ChangeRunState(run);

        navAgent.SetDestination(position);

        SetMoveFlags(true);
    }

    private void StopMoving() {
        currentWayPoint.IsTaken = false;
        SetMoveFlags(false);
    }

    [Task]
    public void MoveToClosestWayPoint(bool run) {
        if (currentWayPoint == null) {
            var closestWayPoint = waypointPathfinder.GetClosestWayPointNode(animalTransform.position);
            currentWayPoint = closestWayPoint;
        }
        else {
            var proposedWayPoint = waypointPathfinder.GetRandomNeighbour(currentWayPoint);
            if (proposedWayPoint == null) {
                Task.current.Fail();
                return;
            }
            else {
                currentWayPoint = proposedWayPoint;
            }
        }

        currentWayPoint.IsTaken = true;
        MoveTo(currentWayPoint.position, run);
    }
}