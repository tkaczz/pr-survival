﻿using System;
using UniRx;
using UnityEngine;

public class TigerController : MonoBehaviour, IController {
    private Subject<Vector3> moveVector;
    public IObservable<Vector3> MoveVector {
        get { return moveVector; }
    }

    private ReactiveProperty<bool> run;
    public ReadOnlyReactiveProperty<bool> Run { get; private set; }

    private Subject<Unit> idle;
    public IObservable<Unit> Idle {
        get { return idle; }
    }

    #region not used, refactor pending

    //not used
    public IObservable<Unit> Jump { get; private set; }

    //not used on tiger
    public ReadOnlyReactiveProperty<BodyPositions> BodyPosition { get; private set; }

    //not used on tiger
    public IObservable<Unit> Take { get; private set; }

    //not used on tiger
    public IObservable<Unit> Use { get; private set; }

    //not used on tiger
    public IObservable<Unit> Inventory { get; private set; }

    //not used on tiger
    public IObservable<Quaternion> XRotation { get; private set; }

    //not used on tiger
    public IObservable<Quaternion> YRotation { get; private set; }

    #endregion not used, refactor pending

    private void Awake() {
        InitMoveVector();
        InitRun();
        InitIdle();
    }

    private void InitMoveVector() {
        moveVector = new Subject<Vector3>();
    }

    private void InitRun() {
        run = new ReactiveProperty<bool>(false);
        Run = new ReadOnlyReactiveProperty<bool>(run);
    }

    private void InitIdle() {
        idle = new Subject<Unit>();
    }

    public void SetIdleState() {
        idle.OnNext(new Unit());
    }

    //trzeba zrobić żeby poruszanie odbywało się w fixed update
    public void MoveCommand(Vector3 deltaVector) {
        moveVector.OnNext(deltaVector);
    }

    //trzeba będzie co klatkę informować że postać biega
    public void ChangeRunState(bool runState) {
        run.Value = runState;
    }
}

public struct AttackMessage {
    public readonly IPawn Pawn;
    public readonly float Damage;

    public AttackMessage(IPawn pawn, float damage) {
        Pawn = pawn;
        Damage = damage;
    }
}