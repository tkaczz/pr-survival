﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using System;

public class TigerAnimations : MonoBehaviour {
    private TigerController tigerController;
    private TigerAttack tigerAttack;
    private Animator tigerAnimator;

    private int runId;
    private int walkId;
    private int idleId;
    private int hitId;

    private void Start() {
        this.tigerController = GetComponent<TigerController>();
        this.tigerAnimator = GetComponent<Animator>();
        this.tigerAttack = GetComponent<TigerAttack>();

        SetIDs();

        //walk
        tigerController.MoveVector
            .Where(state => tigerController.Run.Value == false
                            && tigerAttack.Attacking.Value == false)
            .Subscribe(_ => {
                SetMoveParams(true);
            })
            .AddTo(this);

        //run
        tigerController.MoveVector
            .Where(state => tigerController.Run.Value == true
                            && tigerAttack.Attacking.Value == false)
            .Subscribe(_ => {
                SetMoveParams(false, true);
            })
            .AddTo(this);

        tigerController.Idle
            .Subscribe(_ => {
                SetMoveParams(false);
            })
            .AddTo(this);

        tigerAttack.Attacking
            .Subscribe(state => {
                SetMoveParams(false);
                tigerAnimator.SetBool(hitId, state);
            })
            .AddTo(this);
    }

    private void SetMoveParams(bool walk, bool run = false) {
        tigerAnimator.SetBool(walkId, walk);
        tigerAnimator.SetBool(runId, run);
    }

    private void SetIDs() {
        runId = Animator.StringToHash("Running");
        walkId = Animator.StringToHash("Walking");
        idleId = Animator.StringToHash("Idle");
        hitId = Animator.StringToHash("Hit");
    }
}