﻿using UniRx;
using UnityEngine;

public class TigerPawn : MonoBehaviour, IPawn {
    public NumericalStat Health { get; private set; }
    public ReadOnlyReactiveProperty<bool> IsDead { get; private set; }
    public Subject<float> DamageCommand { get; private set; }

    private void Awake() {
        DamageCommand = new Subject<float>();

        Health = new NumericalStat(
            100,
            100,
            0
        );

        IsDead = Health.Current
            .Select(hp => hp <= Health.Min.Value)
            .ToReadOnlyReactiveProperty();
    }
}