﻿using Panda;
using Player;
using System;
using UniRx;
using UnityEngine;
using Zenject;

public class TigerAttack : MonoBehaviour {
    [SerializeField]
    private Settings settings;
    private Transform animalTransform;
    private TigerController tigerController;
    private PlayerPawn playerPawn;

    [Task]
    private bool isPlayerInAttackRange {
        get {
            var distance = Vector3.Distance(playerPawn.transform.position, animalTransform.position);
            return distance <= settings.PlayerAttackRange;
        }
    }

    [Task]
    private bool isPlayerInHitRange {
        get {
            var distance = Vector3.Distance(playerPawn.transform.position, animalTransform.position);
            //Debug.Log(string.Format("{0} {1} {2}", distance, distance <= settings.PlayerHitRange, settings.PlayerHitRange));
            return distance <= settings.PlayerHitRange;
        }
    }

    [Task]
    public bool isAttacking {
        get {
            return Attacking.Value;
        }
    }

    private ReactiveProperty<bool> attacking;
    public ReadOnlyReactiveProperty<bool> Attacking;

    [Inject]
    public void Construct(PlayerPawn player) {
        this.playerPawn = player;
    }

    private void Awake() {
        animalTransform = GetComponent<Transform>();
        tigerController = GetComponent<TigerController>();

        InitAttacking();
    }

    private void InitAttacking() {
        attacking = new ReactiveProperty<bool>(false);
        Attacking = new ReadOnlyReactiveProperty<bool>(attacking);
    }

    [Task]
    private void SetAttack() {
        attacking.Value = true;
        Task.current.Succeed();
    }

    [Task]
    private void DisableAttack() {
        attacking.Value = false;
        Task.current.Succeed();
    }

    [Task]
    private void AttackPlayer(float dmg) {
        playerPawn.DamageCommand.OnNext(dmg);
        Task.current.Succeed();
    }

    [Serializable]
    public class Settings {
        public float PlayerAttackRange = 4.0f;
        public float PlayerHitRange = 1f;
    }
}