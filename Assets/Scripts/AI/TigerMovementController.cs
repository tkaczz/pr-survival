﻿using Pawn;
using System;
using UniRx;
using UnityEngine;
using Zenject;

public class TigerMovementController : MonoBehaviour, IMovementController {
    [SerializeField]
    private Settings settings;

    private TigerController controller;
    private RigidbodyCharacterController characterController;

    public IObservable<Unit> Jumped { get; private set; }
    public IObservable<Unit> IsLanding { get; private set; }
    public ReadOnlyReactiveProperty<bool> IsRunning { get; private set; }
    public ReadOnlyReactiveProperty<bool> InAir { get; private set; }
    public float SpeedMultipler { get; set; }

    [Inject]
    public void Construct(
        RigidbodyCharacterController rigidbodyCharacterController
    ) {
        this.controller = GetComponent<TigerController>();
        this.characterController = rigidbodyCharacterController;
    }

    private void Awake() {
        controller.MoveVector
            .Subscribe(vector => {
                var run = controller.Run.Value ? settings.RunSpeed : settings.WalkSpeed;

                //Debug.Log("TigerMovement moving");

                MoveTiger(vector * run * Time.deltaTime);
            })
            .AddTo(this);
    }

    //todo trzeba dodać by poruszało się z użyciem poniższej funkcji a nie SetDestination z navmesh

    private void MoveTiger(Vector3 moveVector) {
        characterController.MoveForce(moveVector);
    }

    [Serializable]
    public class Settings {
        public float WalkSpeed = 500;
        public float RunSpeed = 700;
        public float JumpForce = 500;
    }
}