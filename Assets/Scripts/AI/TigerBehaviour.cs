﻿using Panda;
using System;
using UniRx;
using UnityEngine;

/// <summary>
/// Przystawka dla behaviour tree, by łatwiej bylo oksryptować tygryska
/// </summary>
/// todo zmiana nazwy na NonAggresive
public class TigerBehaviour : MonoBehaviour {
    private TigerNavigation ai_mover;
    private PandaBehaviour pandaBehaviour;
    private TigerController tigerController;

    [SerializeField]
    private Settings settings;

    private void Awake() {
        ai_mover = GetComponent<TigerNavigation>();
        pandaBehaviour = GetComponent<PandaBehaviour>();
        tigerController = GetComponent<TigerController>();
    }

    [Task]
    private void MoveToClosestWayPoint() {
        if (ai_mover.inProgressFlag == true && ai_mover.movingFlag == true) {
            //Task.current.Fail();
            return;
        }

        ai_mover.MoveToClosestWayPoint(false);

        Task.current.Succeed();
    }

    [Task]
    private void BeIdle() {
        tigerController.SetIdleState();
        Task.current.Succeed();
    }

    [Serializable]
    private class Settings {
        public float MinIdleWait = 4f;
        public float MaxIdleWait = 10f;
    }
}