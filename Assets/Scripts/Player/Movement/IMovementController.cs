using System;
using UniRx;

public interface IMovementController {
    IObservable<Unit> Jumped { get; }
    IObservable<Unit> IsLanding { get; }
    ReadOnlyReactiveProperty<bool> IsRunning { get; }
    ReadOnlyReactiveProperty<bool> InAir { get; }
    float SpeedMultipler { get; set; }
}