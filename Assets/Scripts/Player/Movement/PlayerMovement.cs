using Pawn;
using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Player {
    public class PlayerMovement : MonoBehaviour, IMovementController {
        private IController controller;
        private Transform playerTransform;
        private RigidbodyCharacterController characterController;

        [SerializeField]
        private Settings settings;

        private bool controlableMoving = true;
        private bool jumpFlag = false;

        private Subject<Unit> jumped;
        public IObservable<Unit> Jumped {
            get { return jumped; }
        }

        public IObservable<Unit> IsLanding { get; private set; }
        public ReadOnlyReactiveProperty<bool> InAir { get; private set; }

        [SerializeField]
        private float speedMultipler = 1.0f;

        public float SpeedMultipler {
            get { return speedMultipler; }
            set { speedMultipler = value; }
        }

        private ReactiveProperty<bool> isRunning;
        public ReadOnlyReactiveProperty<bool> IsRunning { get; private set; }

        [Inject]
        public void Construct(
            RigidbodyCharacterController characterController,
            IController controller,
            [Inject(Id = Transforms.Pawn)] Transform playerTransform
        ) {
            this.characterController = characterController;
            this.controller = controller;
            this.playerTransform = playerTransform;
        }

        private void Awake() {
            InitMove();
            InitJump();
            InitLand();
            InitInAir();
            InitIsRunning();
        }

        private void InitIsRunning() {
            isRunning = new ReactiveProperty<bool>();
            IsRunning = new ReadOnlyReactiveProperty<bool>(isRunning);
        }

        private void InitInAir() {
            InAir = characterController.IsOnGround
                .Select(isOnGround => {
                    return !isOnGround;
                })
                .ToReadOnlyReactiveProperty();
        }

        private void InitLand() {
            // on ground, but jumped before
            IsLanding = characterController.IsOnGround
                .Where(isOnGround => {
                    var check =
                        isOnGround == true &&
                        jumpFlag == true;
                    return check;
                })
                .Select(_ => {
                    return new Unit();
                });

            IsLanding
                .Subscribe(_ => {
                    controlableMoving = true;
                    jumpFlag = false;
                })
                .AddTo(this);
        }

        private void InitJump() {
            // on ground, want to jump
            jumped = new Subject<Unit>();

            controller.Jump
                .Where(_ => {
                    var check =
                        jumpFlag == false &&
                        characterController.IsOnGround.Value == true &&
                        controller.BodyPosition.Value == BodyPositions.Standing;

                    //Debug.Log(string.Format("jump flag {0} isOnGround {1} bodyPosition {2}",
                    //    jumpFlag,
                    //    characterController.IsOnGround.Value,
                    //    controller.BodyPosition.Value)
                    //);

                    return check;
                })
                .Subscribe(_ => {
                    var jumpVector = new Vector3(
                        playerTransform.forward.x, 1, playerTransform.forward.z
                    );

                    //var jumpVector = new Vector3(
                    //    characterController.Velocity.Value.x, 1, characterController.Velocity.Value.z
                    //);

                    characterController.MoveImpulse(jumpVector * settings.JumpForce);
                    jumped.OnNext(new Unit());
                })
                .AddTo(this);

            Jumped
                .Subscribe(_ => {
                    controlableMoving = false;
                    jumpFlag = true;
                })
                .AddTo(this);
        }

        private void InitMove() {
            controller.MoveVector
                .Where(_ => controlableMoving)
                .Subscribe(moveVector => {
                    float run;

                    if (controller.Run.Value == true) {
                        run = settings.RunSpeed;
                        isRunning.Value = true;
                    }
                    else {
                        run = settings.WalkSpeed;
                        isRunning.Value = false;
                    }

                    var vector = moveVector * run * speedMultipler;
                    characterController.MoveForce(vector);
                })
                .AddTo(this);
        }

        [Serializable]
        public class Settings {
            public float WalkSpeed = 500;
            public float RunSpeed = 700;
            public float JumpForce = 500;
        }
    }
}