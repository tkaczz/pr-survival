using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Player {

    public class PlayerController : MonoBehaviour, IController {
        [SerializeField] private Settings settings;

        private IInput input;
        private Transform playerTransform;
        private Transform playerCamera;

        public IObservable<Vector3> MoveVector { get; private set; }
        public IObservable<Unit> Jump { get; private set; }
        public ReadOnlyReactiveProperty<bool> Run { get; private set; }

        public ReadOnlyReactiveProperty<BodyPositions> BodyPosition { get; private set; }

        private Subject<Unit> take;
        public IObservable<Unit> Take { get { return take; } }

        private Subject<Unit> use;
        public IObservable<Unit> Use { get { return use; } }

        private Subject<Unit> inventory;
        public IObservable<Unit> Inventory { get { return inventory; } }

        public IObservable<Quaternion> XRotation { get; private set; }
        public IObservable<Quaternion> YRotation { get; private set; }

        [Inject]
        public void Construct(
            IInput input,
            [Inject(Id = Transforms.Pawn)] Transform playerTransform,
            [Inject(Id = Transforms.PlayerCamera)] Transform cameraTransform
        ) {
            this.input = input;
            this.playerTransform = playerTransform;
            this.playerCamera = cameraTransform;
        }

        private void Awake() {
            InitMoveVector();
            InitRun();
            InitJump();
            InitPosition();
            InitRotation();
            InitTake();
            InitUse();
            InitInventory();
        }

        private void InitInventory() {
            inventory = new Subject<Unit>();

            input.ActionInputs
                .Where(actionInputs => actionInputs.Inventory == true)
                .Subscribe(_ => inventory.OnNext(new Unit()))
                .AddTo(this);
        }

        private void InitTake() {
            take = new Subject<Unit>();

            input.ActionInputs
                .Where(actionInputs => actionInputs.Take == true)
                .Subscribe(_ => take.OnNext(new Unit()))
                .AddTo(this);
        }

        private void InitUse() {
            use = new Subject<Unit>();

            input.ActionInputs
                .Where(actionInputs => actionInputs.Use == true)
                .Subscribe(_ => use.OnNext(new Unit()))
                .AddTo(this);
        }

        private void InitMoveVector() {
            // based on https://gist.github.com/JavadocMD/b9d87c639953e19ea1943f550907b9aa
            MoveVector = input.MoveInputs
                .Where(moveInputs => moveInputs.Movement != Vector2.zero)
                .Select(moveInputs => {
                    var moveVector = moveInputs.Movement;

                    var velocity =
                        moveVector.x * playerTransform.right +
                        moveVector.y * playerTransform.forward;

                    return velocity;
                });
        }

        private void InitRun() {
            Run = input.MoveInputs
                .Select(moveInputs => {
                    return moveInputs.Run;
                })
                .ToReadOnlyReactiveProperty()
                .AddTo(this);
        }

        private void InitJump() {
            Jump = input.MoveInputs
                .Where(moveInputs => moveInputs.Jump)
                .Select(moveInputs => {
                    return new Unit();
                });
        }

        private void InitRotation() {
            XRotation = input.RotationVector
                .Where(inputLook => inputLook != Vector2.zero)
                .Select(inputLook => {
                    var horizontal = inputLook.x * Vector3.up;

                    var horizontalQuaternion =
                        playerTransform.localRotation *
                        Quaternion.Euler(horizontal * Time.deltaTime);

                    return horizontalQuaternion;
                });

            YRotation = input.RotationVector
                .Where(inputLook => inputLook != Vector2.zero)
                .Select(inputLook => {
                    var vertical = inputLook.y * Time.deltaTime * Vector3.left;

                    var verticalQuaternion =
                        playerCamera.localRotation *
                        Quaternion.Euler(vertical);

                    var clamped =
                        ClampRotationAroundXAxis(
                            verticalQuaternion, -settings.MaxViewAngle, -settings.MinviewAngle
                        );

                    return clamped;
                });
        }

        private void InitPosition() {
            var positionObservable = input.MoveInputs
                .Where(moveInputs => moveInputs.PositionChange)
                .Select(moveInputs => {
                    //Debug.Log("Changing");
                    if (BodyPosition.Value == BodyPositions.Standing) {
                        return BodyPositions.Crouch;
                    }
                    else {
                        return BodyPositions.Standing;
                    }
                });

            BodyPosition = new ReadOnlyReactiveProperty<BodyPositions>(
                positionObservable,
                BodyPositions.Standing
            );
        }

        // From StandardAssets functions
        private static Quaternion ClampRotationAroundXAxis(
            Quaternion q,
            float minAngle,
            float maxAngle
        ) {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, minAngle, maxAngle);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        [Serializable]
        public class Settings {
            public float MinviewAngle = -60f;
            public float MaxViewAngle = 90f;
        }
    }
}