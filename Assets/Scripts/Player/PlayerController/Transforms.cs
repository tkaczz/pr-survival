﻿namespace Player {

    /// <summary>
    /// Used in injection
    /// </summary>
    public enum Transforms {
        Pawn,
        PlayerCamera
    }
}