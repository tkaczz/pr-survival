using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Player {

    public class PlayerRotation : MonoBehaviour {
        [SerializeField] private Settings settings;

        private IController pawnController;
        private new Rigidbody rigidbody;

        [Inject]
        public void Construct(
            IController pawnController,
            Rigidbody rigidbody
        ) {
            this.pawnController = pawnController;
            this.rigidbody = rigidbody;
        }

        private void Start() {
            pawnController.XRotation
                .Subscribe(quaternion => {
                    var lerped = Quaternion.Slerp(
                        rigidbody.rotation,
                        quaternion,
                        settings.InterpSpeed
                    );

                    rigidbody.rotation = lerped;
                });
        }

        [Serializable]
        private class Settings {

            [Range(0, 1)]
            public float InterpSpeed = 0.75f;
        }
    }
}