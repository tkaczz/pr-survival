using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Player {

    public class CameraControl : MonoBehaviour {
        [SerializeField] private Settings settings;

        private IController pawnController;
        private PositionController positionController;
        private Transform cameraTransform;

        [Inject]
        public void Construct(
            IController pawnController,
            PositionController positionController,
            [Inject(Id = Transforms.PlayerCamera)]
            Transform playerCameraTransform
        ) {
            this.cameraTransform = playerCameraTransform;
            this.positionController = positionController;
            this.pawnController = pawnController;
        }

        private void Awake() {
            pawnController.YRotation
                .Subscribe(quaternion => {
                    var lerped = Quaternion.Slerp(
                        cameraTransform.localRotation,
                        quaternion,
                        settings.InterpSpeed
                    );

                    cameraTransform.localRotation = lerped;
                })
                .AddTo(this);

            positionController.CurrentHeightMultipler
                .Subscribe(nextHeight => {
                    var newCameraHeight = new Vector3(0, nextHeight, 0);

                    cameraTransform.localPosition = newCameraHeight;
                })
                .AddTo(this);
        }

        [Serializable]
        private class Settings {

            [Range(0, 1)]
            public float InterpSpeed = 0.75f;
        }
    }
}