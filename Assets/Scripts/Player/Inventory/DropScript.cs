﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class DropScript : MonoBehaviour {
    [SerializeField] private float freezeVelocity = 0.5f;
    [SerializeField] private float startInterval = 3.0f;
    [SerializeField] private float checkInterval = 1.0f;

    private new Rigidbody rigidbody;

    private void Awake() {
        SetupDeps();
    }

    private void OnEnable() {
        //Debug.Log("Enable");
        InitDrop();
    }

    private void SetupDeps() {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void InitDrop() {
        Observable.Timer(
            TimeSpan.FromSeconds(startInterval),
            TimeSpan.FromSeconds(checkInterval)
        )
        .TakeWhile(_ => {
            //Debug.Log("Tick");
            if (rigidbody.velocity.magnitude < freezeVelocity) {
                //Debug.Log("Correct");
                FreezeObject();
                return false;
            }
            else { return true; }
        });
        //poniższe jest w celu sprawdzenia czy nie będzie wycieków
        //i nie ma
        //.Subscribe(
        //    (_) => Debug.Log("Finished"),
        //    (_) => Debug.Log("Completed")
        //);
    }

    private void FreezeObject() {
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }
}