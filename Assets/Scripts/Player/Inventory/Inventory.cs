﻿using System;
using Player;
using UniRx;
using UnityEngine;
using Zenject;

public struct DroppedItemMessage {
    public readonly ItemTypes ItemType;
    public readonly int Quantity;
    public readonly GameObject ItemObject;

    public DroppedItemMessage(
        ItemTypes itemType,
        int quantity,
        GameObject itemObject
    ) {
        this.ItemType = itemType;
        this.Quantity = quantity;
        this.ItemObject = itemObject;
    }
}
public class Inventory : MonoBehaviour {
    private Transform playerTransform;
    private PickupHandler pickupHandler;
    private ItemsDataBase itemsDataBase;
    private CustomItemFactory itemFactory;

    [SerializeField] private float maxInventoryWeight = 25.0f;
    [SerializeField] private float dropDistance = 1f;

    private Subject<DroppedItemMessage> droppedItem;
    public ReactiveDictionary<ItemTypes, int> Items { get; private set; }
    public NumericalStat Weight { get; private set; }
    public IObservable<DroppedItemMessage> DroppedItem {
        get { return droppedItem; }
    }

    private void Awake() {
        InitItemsList();
        InitInventoryWeight();
        InitItemPickup();
        InitItemDrop();
    }

    private void InitItemsList() {
        Items = new ReactiveDictionary<ItemTypes, int>();
    }

    private void InitInventoryWeight() {
        Weight = new NumericalStat(0, maxInventoryWeight, 0);

        //dodawanie nowego przedmiotu
        Items
            .ObserveAdd()
            .Subscribe(newItem => {
                Weight.UnaryCurrent(CalculateItemWeight(newItem.Key, newItem.Value));
                //Debug.Log("Add " + Weight.Current.Value);
            })
            .AddTo(this);

        //usuwanie przedmiotu
        Items
            .ObserveRemove()
            .Subscribe(removedItem => {
                Weight.UnaryCurrent(-CalculateItemWeight(removedItem.Key, removedItem.Value));
                //Debug.Log("Remove " + Weight.Current.Value);
            })
            .AddTo(this);

        //zmiana ilości
        Items
            .ObserveReplace()
            .Subscribe(changedItem => {
                var diffCount = changedItem.NewValue - changedItem.OldValue;
                var diffWeight = CalculateItemWeight(changedItem.Key, diffCount);
                Weight.UnaryCurrent(diffWeight);
                //Debug.Log("Replace " + Weight.Current.Value);
            })
            .AddTo(this);
    }

    private void InitItemPickup() {
        pickupHandler.PickupEvent
            .Subscribe(pickedUpItem => {
                TakeItem(pickedUpItem);
            })
            .AddTo(this);
    }

    private bool TakeItem(ItemTypes itemType, int quantity) {
        //najpierw trzeba przeliczyć czy przedmiot nie jest za ciężki
        var weight = itemsDataBase.FindItemRow(itemType).Weight;
        if (CheckIfCanBePicked(weight) == false) { return false; }

        //nie jest więc idziemy dalej
        //sprawdzenie czy gracz ma już tego typu przedmiot w plecaku
        var itemCheck = CheckForItemInInventory(itemType);

        //ma więc, trzeba dodać pewną ilość
        if (itemCheck) {
            Items[itemType] += quantity;
        }
        //nie ma więc trzeba utworzyć nowy wpis
        else {
            Items.Add(itemType, quantity);
        }

        return true;
    }

    private bool TakeItem(Item pickedUpItem) {
        var check = TakeItem(pickedUpItem.ItemType, pickedUpItem.Quantity);
        if (check == true) {
            DestroyItem(pickedUpItem);
            return true;
        }

        return false;
    }

    private void DestroyItem(Item pickedUpItem) {
        Destroy(pickedUpItem.gameObject);
    }

    private void InitItemDrop() {
        droppedItem = new Subject<DroppedItemMessage>();

        droppedItem
            .Subscribe(message => {
                var itemPosition =
                    playerTransform.position + (playerTransform.forward.normalized * dropDistance);

                message.ItemObject.transform.position = itemPosition;
            })
            .AddTo(this);
    }

    private float CalculateItemWeight(ItemTypes itemType, int quantity) {
        var itemRow = itemsDataBase.FindItemRow(itemType);
        return itemRow.Weight * quantity;
    }

    private bool CheckForItemInInventory(ItemTypes itemKey) {
        return Items.ContainsKey(itemKey);
    }

    [Inject]
    public void Construct(
        [Inject(Id=Transforms.Pawn)]
        Transform playerTransform,
        PickupHandler pickupHandler,
        ItemsDataBase itemsDataBase,
        CustomItemFactory itemFactory
    ) {
        this.playerTransform = playerTransform;
        this.pickupHandler = pickupHandler;
        this.itemsDataBase = itemsDataBase;
        this.itemFactory = itemFactory;
    }

    public void DecreseItemCount(ItemTypes itemType, int quantity) {
        if (CheckForItemInInventory(itemType) == false) { return; }

        var check = Items[itemType] - quantity;
        if (check > 0) {
            Items[itemType] -= quantity;
        }
        else if (check == 0) {
            Items.Remove(itemType);
        }
        else {
            return;
        }
    }

    /// <summary>
    /// Próbuje przenieść przedmiot, jeśli się nie uda to tworzy instancję i upuszcza blisko postaci
    /// </summary>
    /// <returns>Status czy udało się przenieść przedmiot</returns>
    public bool TransferItem(ItemTypes itemType, int quantity) {
        var check = TakeItem(itemType, quantity);
        if (check == false) {
            var item = itemFactory.Create(itemType, quantity).gameObject;
            droppedItem.OnNext(new DroppedItemMessage(itemType, quantity, item));
            return false;
        }

        return true;
    }

    public bool CheckIfCanBePicked(float weight) {
        if ((Weight.Current.Value + weight) > maxInventoryWeight) {
            return false;
        }
        else {
            return true;
        }
    }

    public void DropItem(ItemTypes itemType, int quantityToDrop) {
        //upuszczamy taką ilość że już nie mamy żadnego tego typu przedmiotu w ekwipunku
        if (Items[itemType] - quantityToDrop <= 0) {
            var newItemObject = itemFactory.Create(itemType, Items[itemType]);

            droppedItem.OnNext(
                new DroppedItemMessage(
                    itemType,
                    Items[itemType],
                    newItemObject.gameObject)
                );
            Items.Remove(itemType);
        }
        //pomniejszamy o pewną ilość
        else {
            Items[itemType] -= quantityToDrop;
            var newItemObject = itemFactory.Create(itemType, Items[itemType]);

            droppedItem.OnNext(
                new DroppedItemMessage(
                    itemType,
                    Items[itemType],
                    newItemObject.gameObject)
                );
        }
    }
}