using UniRx;
using UnityEngine;
using System;
using Zenject;

namespace Player {

    public sealed class PlayerPawn : MonoBehaviour, IPawn {
        private Settings settings;

        public NumericalStat Health { get; private set; }
        public ReadOnlyReactiveProperty<bool> IsDead { get; private set; }

        // od czegoś trzeba zacząć, później to dopracuję; koncept wydaje się ok
        public Subject<float> DamageCommand { get; private set; }

        [Inject]
        public void Construct(Settings settings) {
            this.settings = settings;
        }

        private void Awake() {
            DamageCommand = new Subject<float>();

            Health = new NumericalStat(
                settings.Health.StartValue,
                settings.Health.MaxValue,
                settings.Health.MinValue
            );

            IsDead = Health.Current
                .Select(hp => hp <= Health.Min.Value)
                .ToReadOnlyReactiveProperty();
        }

        [Serializable]
        public class Settings {
            public NumericalStatInfo Health;
        }
    }
}