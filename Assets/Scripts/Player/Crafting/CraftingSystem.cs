﻿using UnityEngine;
using Zenject;

public class CraftingSystem : MonoBehaviour {
    private ItemsDataBase itemsDatabase;
    private Inventory inventory;

    [Inject]
    public void Construct(
        ItemsDataBase itemsDataBase,
        Inventory playersInventory
    ) {
        this.itemsDatabase = itemsDataBase;
        this.inventory = playersInventory;
    }

    public void CraftItems(RecipesDatabase.Recipe recipe) {
        if (CheckIngridientsWithInventory(recipe) == false) { return; }

        foreach (var product in recipe.RecipeProducts) {
            inventory.TransferItem(product.Key, product.Value);
        }

        foreach (var ingridient in recipe.Ingridients) {
            inventory.DecreseItemCount(ingridient.Key, ingridient.Value);
        }
    }

    private bool CheckIngridientsWithInventory(RecipesDatabase.Recipe recipe) {
        foreach (var recipeIngridient in recipe.Ingridients) {
            //sprawdzenie czy jest taki przedmiot
            if (inventory.Items.ContainsKey(recipeIngridient.Key) == false) { return false; }

            //a potem jego ilości
            var itemCount = inventory.Items[recipeIngridient.Key] - recipeIngridient.Value;
            if (itemCount < 0) { return false; }
        }

        return true;
    }
}