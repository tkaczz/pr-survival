using UnityEngine;

// based on https://ornithoptergames.com/reactivex-and-unity3d-part-3/
public struct MoveInputs {
    public readonly Vector2 Movement;
    public readonly bool Run;
    public readonly bool Jump;
    public readonly bool PositionChange;

    public MoveInputs(
        Vector2 movement,
        bool run,
        bool jump,
        bool positionChange
    ) {
        this.Movement = movement;
        this.Run = run;
        this.Jump = jump;
        this.PositionChange = positionChange;
    }
}