using Global;
using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;

namespace Player {

    /// <summary>
    /// Implementacja IInput w oparciu o klawiatur� i myszk�
    /// </summary>
    // na dobr� spraw� nie tylko to
    public class KeyboardMouseInputHandler : MonoBehaviour, IInput {
        [SerializeField] private Settings settings;

        private FrameObservables frameObservables;

        public IObservable<Vector2> RotationVector { get; private set; }
        public IObservable<MoveInputs> MoveInputs { get; private set; }
        public IObservable<ActionInputs> ActionInputs { get; private set; }

        [Inject]
        public void Construct(FrameObservables frameObservables) {
            this.frameObservables = frameObservables;
        }

        private void Awake() {
            InitMoveInputs();
            InitRotationInput();
            InitActionInputs();
        }

        private void InitRotationInput() {
            RotationVector = frameObservables.FixedUpdateAsObservable()
                .Select(_ => {
                    var x = Input.GetAxis("Mouse X") * settings.MouseXSensivity;
                    var y = Input.GetAxis("Mouse Y") * settings.MouseYSensivity;

                    var vector = new Vector2(x, y);
                    return vector;
                });
        }

        private void InitMoveInputs() {
            var jump = frameObservables.UpdateAsObservable()
                .Where(_ => Input.GetButton("Jump"));
            var jumpLatch = CustomObervables.Latch(
                frameObservables.FixedUpdateAsObservable(),
                jump,
                false
            );

            var changePosition = frameObservables.UpdateAsObservable()
                .Where(_ => Input.GetButtonDown("ChangePosition"));
            var changePositionLatch = CustomObervables.Latch(
                frameObservables.FixedUpdateAsObservable(),
                changePosition,
                false
            );

            var run = frameObservables.UpdateAsObservable()
                .Where(_ => Input.GetButton("Run"));
            var runLatch = CustomObervables.Latch(
                frameObservables.FixedUpdateAsObservable(),
                run,
                false
            );

            //to b�dzie si� wi�za�o z symulacjami fizycznymi
            //wi�c powinno si� to samplowa� w fixedUpdate
            var move = frameObservables.FixedUpdateAsObservable()
                .Select(_ => {
                    var x = Input.GetAxis("Horizontal");
                    var y = Input.GetAxis("Vertical");

                    //by posta� nie przemieszcza�a si�z byt szybko
                    //id�c po skosach
                    var vector = new Vector2(x, y).normalized;
                    return vector;
                });

            MoveInputs = Observable.Zip(
                move, runLatch, jumpLatch, changePositionLatch,
                (a, b, c, d) => new MoveInputs(a, b, c, d));
        }

        private void InitActionInputs() {
            var take = frameObservables.UpdateAsObservable()
                .Select(_ => {
                    return Input.GetButtonDown("Take");
                });

            var use = frameObservables.UpdateAsObservable()
                .Select(_ => {
                    return Input.GetButtonDown("Use");
                });

            var inventory = frameObservables.UpdateAsObservable()
                .Select(_ => {
                    return Input.GetButtonDown("Inventory");
                });

            var exitMenu = frameObservables.UpdateAsObservable()
                .Select(_ => {
                    return Input.GetButtonDown("ExitMenu");
                });

            ActionInputs = Observable.Zip(
                take, use, inventory, exitMenu,
                (a, b, c, d) => new ActionInputs(a, b, c, d));
        }

        [Serializable]
        public class Settings {
            public float MouseXSensivity;
            public float MouseYSensivity;
        }
    }
}