using System;
using UnityEngine;

/// <summary>
/// Interfejs zapewniaj�cy kontrakt do klasy
/// zapewniaj�cej otrzymywanie wej�cia z r�znych urz�dze�
/// </summary>
public interface IInput {
    /// <summary>
    /// Np. przesuni�cie myszki, ga�ki itp.
    /// </summary>
    IObservable<Vector2> RotationVector { get; }

    /// <summary>
    /// Kierunki poruszanai si�, skok, pozycja itp.
    /// </summary>
    IObservable<MoveInputs> MoveInputs { get; }

    /// <summary>
    /// Pozosta�e typu u�yj, wzi�� co� itp.
    /// </summary>
    IObservable<ActionInputs> ActionInputs { get; }
}