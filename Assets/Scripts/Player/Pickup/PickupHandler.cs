﻿using System;
using Global;
using Player;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;

public enum ObjectTypes {
    Null,
    Item,
    Chest
}

public struct ObjectInRangeMessage {
    public readonly ObjectTypes ObjectType;
    public GameObject Object { get; private set; }

    public ObjectInRangeMessage(ObjectTypes objectType, GameObject @object) : this() {
        ObjectType = objectType;
        Object = @object;
    }
}

public class PickupHandler : MonoBehaviour {
    private Transform playerCameraTransform;
    private IController playerController;

    [SerializeField]
    private float pickupRayLegth = 0.5f;

    private int objectsLayer;
    private GameObject currentInSightObject;
    private FrameObservables frameObservable;

    private Subject<ObjectInRangeMessage> objectInRange;

    private Subject<Item> pickupSubject;
    private Subject<IOpenable> openableSubject;

    /// <summary>
    /// Can push null
    /// </summary>
    public IObservable<ObjectInRangeMessage> ObjectInRange { get { return objectInRange; } }

    public IObservable<Item> PickupEvent { get { return pickupSubject; } }
    public IObservable<IOpenable> OpenableEvent { get { return openableSubject; } }

    private void Awake() {
        InitLayers();
        InitInSight();
        InitPickup();
        InitOpenable();
    }

    private void InitOpenable() {
        openableSubject = new Subject<IOpenable>();

        playerController.Use
            .Subscribe(_ => {
                var itemCheck = RaycastForObjects();

                if (itemCheck == null) { return; }

                if (itemCheck.layer == LayerMask.NameToLayer("Chest")) {
                    openableSubject.OnNext(itemCheck.GetComponent<IOpenable>());
                }
            })
            .AddTo(this);
    }

    private void InitLayers() {
        var itemsLayer = 1 << LayerMask.NameToLayer("Item");
        var chestLayer = 1 << LayerMask.NameToLayer("Chest");
        objectsLayer = itemsLayer | chestLayer;
    }

    private void InitInSight() {
        objectInRange = new Subject<ObjectInRangeMessage>();

        frameObservable.UpdateAsObservable()
            .Subscribe(_ => {
                var newItemInSight = RaycastForObjects();

                if (!ReferenceEquals(newItemInSight, currentInSightObject)) {
                    currentInSightObject = newItemInSight;

                    var objectType = GetObjectType(newItemInSight);
                    objectInRange.OnNext(new ObjectInRangeMessage(objectType, newItemInSight));
                }
            })
            .AddTo(this);
    }

    private ObjectTypes GetObjectType(GameObject newItemInSight) {
        if (newItemInSight == null) {
            return ObjectTypes.Null;
        }
        else if (newItemInSight.layer == LayerMask.NameToLayer("Item")) {
            return ObjectTypes.Item;
        }
        else {
            return ObjectTypes.Chest;
        }
    }

    private void InitPickup() {
        pickupSubject = new Subject<Item>();

        playerController.Take
            .Subscribe(_ => {
                var itemCheck = RaycastForObjects();

                if (itemCheck == null) { return; }

                if (itemCheck.layer == LayerMask.NameToLayer("Item")) {
                    pickupSubject.OnNext(itemCheck.GetComponent<Item>());
                }
            })
            .AddTo(this);
    }

    private GameObject RaycastForObjects() {
        RaycastHit raycastHit;

        var check = Physics.Raycast(
            playerCameraTransform.position,
            playerCameraTransform.forward,
            out raycastHit,
            pickupRayLegth,
            objectsLayer,
            QueryTriggerInteraction.Collide
        );

        if (check) {
            //Debug.Log("Hit"); //niby działa ok przy skrzynkach i przedmiotach
            return raycastHit.transform.gameObject;
        }
        else {
            //lepiej zwrócić null, jest to oznaka że niczego nie ma w zasięgu wzroku
            return null;
        }
    }

    [Inject]
    public void Construct(
        [Inject(Id = Transforms.PlayerCamera)]
        Transform playerCameraTransform,
        IController playerController,
        FrameObservables frameObservable
    ) {
        this.playerCameraTransform = playerCameraTransform;
        this.playerController = playerController;
        this.frameObservable = frameObservable;
    }
}