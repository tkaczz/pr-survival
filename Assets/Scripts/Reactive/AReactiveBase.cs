using System;
using UniRx;

public abstract class AObserverBase : IDisposable {
    protected CompositeDisposable disposables = new CompositeDisposable();

    public virtual void Dispose() {
        disposables.Clear();
    }
}