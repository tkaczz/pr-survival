﻿using Global;
using System;
using UniRx;
using UnityEngine;
using Zenject;

public class CursorScript : MonoBehaviour {
    public BoolReactiveProperty IsLocked;
    public BoolReactiveProperty IsVisible;

    private MainViewPresenter mainViewPresenter;
    private ExitMenuPresenter exitMenuPresenter;
    private GameMode gameMode;

    [Inject]
    public void Construct(
        MainViewPresenter mainViewPresenter,
        ExitMenuPresenter exitMenuPresenter,
        GameMode gameMode
    ) {
        this.mainViewPresenter = mainViewPresenter;
        this.exitMenuPresenter = exitMenuPresenter;
        this.gameMode = gameMode;
    }

    private void Start() {
        InitLockedStatus();
        InitVisibleStatus();
        SetOnGUI();
        InitOnEndGame();
    }

    private void InitOnEndGame() {
        gameMode.WorldState
            .Where(state => state == WorldStates.Ending)
            .Subscribe(_ => {
                SetCursor(true);
            })
            .AddTo(this);
    }

    private void SetOnGUI() {
        mainViewPresenter.IsVisible
            .Subscribe(isVisible => {
                SetCursor(isVisible);
            })
            .AddTo(this);

        //dry brzydko ale trudno
        exitMenuPresenter.IsVisible
            .Subscribe(isVisible => {
                SetCursor(isVisible);
            })
            .AddTo(this);
    }

    private void SetCursor(bool isVisible) {
        IsVisible.Value = isVisible;
        IsLocked.Value = !isVisible;
    }

    private void InitLockedStatus() {
        bool isLocked = false;
        //to i tak nie działa
        //#if UNITY_STANDALONE && UNITY_EDITOR
        //        isLocked = false;
        //#endif
        //#if UNITY_STANDALONE && !UNITY_EDITOR
        //        isLocked = true;
        //#endif

        IsLocked = new BoolReactiveProperty(isLocked);

        IsLocked
            .Subscribe(lockedStatus => {
                SetLockStatus(lockedStatus);
            })
            .AddTo(this);
    }

    private void InitVisibleStatus() {
        bool isVisible = false;

        //to i tak nie działa
        //#if UNITY_STANDALONE && UNITY_EDITOR
        //        isVisible = true;
        //#endif
        //#if UNITY_STANDALONE && !UNITY_EDITOR
        //        isVisible = false;
        //#endif

        IsVisible = new BoolReactiveProperty(isVisible);

        IsVisible
            .Subscribe(visible => {
                SetVisibleStatus(visible);
            })
            .AddTo(this);
    }

    private void SetVisibleStatus(bool isVisible) {
        Cursor.visible = isVisible;
    }

    private void SetLockStatus(bool val) {
        if (val == true) { Cursor.lockState = CursorLockMode.Locked; }
        else { Cursor.lockState = CursorLockMode.None; }
    }
}