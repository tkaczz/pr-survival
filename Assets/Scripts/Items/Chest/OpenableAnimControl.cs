﻿using UniRx;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class OpenableAnimControl : MonoBehaviour {
    private IOpenable openable;
    private Animator animator;

    private void Start() {
        GetDeps();
        InitStates();
    }

    private void InitStates() {
        var isOpened = Animator.StringToHash("IsOpened");

        openable.IsOpen
            .Subscribe(state => {
                animator.SetBool(isOpened, state);
            })
            .AddTo(this);
    }

    private void GetDeps() {
        openable = GetComponent<IOpenable>();
        animator = GetComponent<Animator>();
    }
}