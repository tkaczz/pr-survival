﻿using UniRx;
using UnityEngine;

public class ChestOpener : MonoBehaviour, IOpenable {
    private ReactiveProperty<bool> isOpen;
    public ReadOnlyReactiveProperty<bool> IsOpen { get; private set; }

    public void Close() {
        isOpen.Value = false;
    }

    public void Open() {
        isOpen.Value = true;
    }

    public void Toggle() {
        isOpen.Value = !isOpen.Value;
    }

    private void Awake() {
        InitIsOpen();
    }

    private void InitIsOpen() {
        isOpen = new ReactiveProperty<bool>();
        IsOpen = new ReadOnlyReactiveProperty<bool>(isOpen, false);
    }
}

public interface IOpenable {
    ReadOnlyReactiveProperty<bool> IsOpen { get; }

    void Toggle();

    void Open();

    void Close();
}