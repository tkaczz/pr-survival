﻿using UnityEngine;
using UniRx;
using Zenject;

public class OpenableOpener : MonoBehaviour {
    private PickupHandler pickupHandler;

    [Inject]
    public void Construct(
        PickupHandler pickupHandler
    ) {
        this.pickupHandler = pickupHandler;
    }

    private void Awake() {
        pickupHandler.OpenableEvent
            .Subscribe(openable => {
                openable.Toggle();
            })
            .AddTo(this);
    }
}