﻿using System;
using UnityEngine;
using Zenject;

public class CustomItemFactory : IFactory<ItemTypes, int, Item> {
    private readonly DiContainer diContainer;
    private readonly ItemsDataBase Database;

    public CustomItemFactory(
        DiContainer diContainer,
        ItemsDataBase itemsDataBase
    ) {
        this.diContainer = diContainer;
        this.Database = itemsDataBase;
    }

    /// <summary>
    /// Use item.gameobject to retrieve gameobject
    /// </summary>
    public Item Create(ItemTypes itemType, int amount) {
        var itemFromDatabase = Database.FindItemRow(itemType);

        if (itemFromDatabase != null) {
            var item = diContainer.InstantiatePrefabForComponent<Item>(itemFromDatabase.Prefab);
            item.ItemType = itemType;
            item.Quantity = amount;
            return item;
        }
        else {
            Debug.LogError("Not found in items database"); //temp
            return null;
        }
    }
}