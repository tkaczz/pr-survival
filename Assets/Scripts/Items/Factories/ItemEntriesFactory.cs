﻿using Noesis;

//todo pomyśleć nad cachem
public class ItemEntriesFactory {
    private readonly ItemsDataBase itemsDatabase;

    public ItemEntriesFactory(ItemsDataBase itemsDatabase) {
        this.itemsDatabase = itemsDatabase;
    }

    public ItemEntry CreateItemEntry(ItemTypes itemType, int quantity) {
        var itemFromDB = itemsDatabase.FindItemRow(itemType);

        return new ItemEntry(
            itemFromDB.PrimaryKey,
            new TextureSource(itemFromDB.Thumbnail),
            quantity,
            itemFromDB.Description
        );
    }
}
