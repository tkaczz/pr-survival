﻿/// <summary>
/// Being used as a key
/// </summary>
public enum ItemTypes {
    Ball,
    GreenBall,
    RedBall,
    BigBall
}