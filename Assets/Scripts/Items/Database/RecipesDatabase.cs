﻿using RotaryHeart.Lib.SerializableDictionary;
using System;
using System.Collections.Generic;
using UnityEngine;

//todo można by dodać metodę srawdzającą czy z danych przedmiotów można coś zrobić (bez alokacji tymczasowych obiektów)
[Serializable]
public class RecipesDatabase {
    public Recipe[] Recipes;

    /// <summary>
    /// Sprawdza czy na podstawie podanych składników można cokolwiek zrobić
    /// </summary>
    /// <param name="pickedItems"></param>
    public bool CheckIfSomethingCanBeCreated(IDictionary<ItemTypes, int> pickedItems) {
        if (pickedItems == null || pickedItems.Count == 0) {
            return false;
        }

        var recipesCount = Recipes.Length;
        for (int i = 0; i < recipesCount; i++) {
            if (CheckIfIngridientsMatchRecipe(pickedItems, Recipes[i])) {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Sprawdza na podstawie wybranych przedmiotów, czy można z nich zrobić jakikolwiek przedmiot
    /// Funkcja sprawdza czy podano prawidłową ich ilość
    /// </summary>
    /// <param name="pickedItems">Wybrane przez użytkownika przedmioty</param>
    /// <returns>Listę możliwych receptur/przedmiotów do zrobienia</returns>
    public List<Recipe> FindMatchingRecipes(IDictionary<ItemTypes, int> pickedItems) {
        if (pickedItems == null || pickedItems.Count == 0) {
            return null;
        }
        var result = new List<Recipe>();

        var recipesCount = Recipes.Length;
        for (int i = 0; i < recipesCount; i++) {
            if (CheckIfIngridientsMatchRecipe(pickedItems, Recipes[i])) {
                result.Add(Recipes[i]);
            }
        }

        return result;
    }

    /// <summary>
    /// Sprawdza czy składniki pasują do danej receptury
    /// </summary>
    /// <param name="ingridients">Lista składników</param>
    /// <param name="recipe">Sprawdzana receptura</param>
    /// <returns>Tak/Nie czy składniki pasują do receptury</returns>
    private bool CheckIfIngridientsMatchRecipe(IDictionary<ItemTypes, int> ingridients, Recipe recipe) {
        if (ingridients.Count != recipe.Ingridients.Count) {
            //nie zgadza się ilość więc nie ma co dalej sprawdzać
            return false;
        }

        ////ilość się zgadza
        ////więc sprawdzamy czy wybrane składniki są w danej recepturze
        foreach (var item in ingridients) {
            //czy w ogóle jest
            if (recipe.Ingridients.ContainsKey(item.Key) == false) {
                return false;
            }
            //jeśli tak to czy odpowiednia ilość
            if (recipe.Ingridients[item.Key] > item.Value) {
                return false;
            }
        }

        //nie było przeciwskazań więc zwracamy prawdę
        return true;
    }

    [Serializable]
    public class Recipe {

        //nieco łatwiej będzie można się nawigować
        [SerializeField]
        private string name;

        //potrzebna jest LISTA składników
        public CraftingList Ingridients;

        //wynikiem jest LISTA produktów
        public CraftingList RecipeProducts;
    }
}

[Serializable]
public class CraftingList : SerializableDictionaryBase<ItemTypes, int> { }