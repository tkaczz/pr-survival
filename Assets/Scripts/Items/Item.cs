﻿using UnityEngine;
using Zenject;

public class Item : MonoBehaviour {
	public ItemTypes ItemType;
	public int Quantity = 1;
}

public class ItemFactory : Factory<ItemTypes, int, Item> { }