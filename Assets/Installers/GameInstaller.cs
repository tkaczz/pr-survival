using UnityEngine;
using Zenject;
using Global;
using System;

public class GameInstaller : MonoInstaller<GameInstaller> {
    [SerializeField] private GameObject playerPrefab;

    [Header("Global")]
    [SerializeField] private GameObject gameModeObject;

    [Header("Misc")]
    [SerializeField] private GameObject miscObject;

    [Header("Presenters")]
    [SerializeField] private GameObject mainViewPresenterObject;

    [SerializeField] private GameObject inventoryPresenterObject;
    [SerializeField] private GameObject exitGamePresenterObject;

    [Header("Crafting")]
    [SerializeField] private GameObject craftingSystemObject;

    [Header("Waypoints system")]
    [SerializeField] private GameObject waypointsSystemObject;

    public override void InstallBindings() {
        BindGameMode();
        BindMisc();
        BindWorldInit();
        BindItemFactory();
        BindPresenters();
        BindCraftSystem();
        BindWayPoints();
    }

    private void BindWayPoints() {
        Container.Bind<WaypointPathfinder>()
            .FromInstance(waypointsSystemObject.GetComponent<WaypointPathfinder>());
    }

    private void BindCraftSystem() {
        Container.Bind<CraftingSystem>()
            .FromInstance(craftingSystemObject.GetComponent<CraftingSystem>());
    }

    private void BindPresenters() {
        Container.Bind<ItemEntriesFactory>().AsSingle();

        Container.Bind<MainViewPresenter>()
            .FromInstance(mainViewPresenterObject.GetComponent<MainViewPresenter>());

        Container.Bind<InventoryPresenter>()
            .FromInstance(inventoryPresenterObject.GetComponent<InventoryPresenter>());

        Container.Bind<CraftingPresenter>()
            .FromInstance(inventoryPresenterObject.GetComponent<CraftingPresenter>());

        Container.Bind<ExitMenuPresenter>()
            .FromInstance(exitGamePresenterObject.GetComponent<ExitMenuPresenter>());

        //related commands
        Container.BindInterfacesAndSelfTo<DropItemCommand>().AsSingle();
        Container.BindInterfacesAndSelfTo<DeselectAllCommand>().AsSingle();
        Container.BindInterfacesAndSelfTo<SwitchToCraftingCommand>().AsSingle();
        Container.BindInterfacesAndSelfTo<SwitchToInventoryViewCommand>().AsSingle();
        Container.BindInterfacesAndSelfTo<CraftCommand>().AsSingle();
        Container.BindInterfacesAndSelfTo<ExitGameCommand>().AsSingle();
    }

    private void BindGameMode() {
        Container.Bind<GameMode>()
            .FromInstance(gameModeObject.GetComponent<GameMode>());

        Container.BindInterfacesAndSelfTo<PlayerUtilities>().AsSingle();
    }

    private void BindMisc() {
        Container.Bind<FrameObservables>()
            .FromInstance(miscObject.GetComponent<FrameObservables>());
    }

    private void BindWorldInit() {
        Container.Bind<WorldInit>()
            .FromInstance(miscObject.GetComponent<WorldInit>())
            .AsSingle();
    }

    private void BindItemFactory() {
        Container.Bind<CustomItemFactory>().AsSingle();
        Container.BindFactory<ItemTypes, int, Item, ItemFactory>()
            .FromFactory<CustomItemFactory>();
    }
}