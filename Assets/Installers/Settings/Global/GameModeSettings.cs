using UnityEngine;
using Zenject;
using Global;

[CreateAssetMenu(fileName = "GameModeSettings", menuName = "Installers/GameModeSettings")]
public class GameModeSettings : ScriptableObjectInstaller<GameModeSettings> {
    public GameMode.Settings GameMode;

    public override void InstallBindings() {
        Container.BindInstances(GameMode);
    }
}