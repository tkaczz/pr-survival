using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "HumanPawnStatsInstaller", menuName = "Installers/HumanPawnStatsInstaller")]
public class HumanPawnStatsInstaller : ScriptableObjectInstaller<HumanPawnStatsInstaller> {
    public HumanPawnStatsSettings humanPawnStatsSettings;

    public override void InstallBindings() {
        Container.BindInstances(humanPawnStatsSettings);
    }
}