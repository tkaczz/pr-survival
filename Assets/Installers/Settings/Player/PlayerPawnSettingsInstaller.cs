using UnityEngine;
using Zenject;
using Player;

[CreateAssetMenu(fileName = "PlayerPawnSettingsInstaller", menuName = "Installers/PlayerPawnSettingsInstaller")]
public class PlayerPawnSettingsInstaller : ScriptableObjectInstaller<PlayerPawnSettingsInstaller> {
    public PlayerPawn.Settings PlayerPawn;

    public override void InstallBindings() {
        Container.BindInstance(PlayerPawn);
    }
}