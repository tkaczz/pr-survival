using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ItemsDataBaseSettings", menuName = "Installers/ItemsDataBaseSettings")]
public class ItemsDataBaseSettings : ScriptableObjectInstaller<ItemsDataBaseSettings> {
    public ItemsDataBase itemsDataBase;

    public override void InstallBindings() {
        Container.BindInstances(itemsDataBase);
    }
}