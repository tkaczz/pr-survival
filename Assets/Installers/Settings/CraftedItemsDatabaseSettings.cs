using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "CraftedItemsDatabaseSettings", menuName = "Installers/CraftedItemsDatabaseSettings")]
public class CraftedItemsDatabaseSettings : ScriptableObjectInstaller<CraftedItemsDatabaseSettings> {
    public RecipesDatabase recipesDatabase;

    public override void InstallBindings() {
        Container.BindInstances(recipesDatabase);
    }
}