using Pawn;
using Player;
using UnityEngine;
using Zenject;

public class TigerInstaller : MonoInstaller<PlayerInstaller> {
    [SerializeField] private Transform tigerTransform;

    public override void InstallBindings() {
        BindTigerController();
        BindTigerPawn();
        BindRigidbodyController();
        BindTigerMovement();
        BindStats();
    }

    private void BindTigerController() {
        Container.Bind<IController>()
            .To<TigerController>()
            .FromInstance(GetComponent<TigerController>());

        Container.Bind<Transform>()
            .WithId(Transforms.Pawn)
            .FromInstance(tigerTransform);
    }

    private void BindTigerPawn() {
        Container.Bind<IPawn>()
            .To<TigerPawn>()
            .FromInstance(GetComponent<TigerPawn>());
    }

    private void BindRigidbodyController() {
        Container.Bind<RigidbodyCharacterController>()
            .FromInstance(GetComponent<RigidbodyCharacterController>());

        Container.Bind<Rigidbody>()
            .FromInstance(GetComponent<Rigidbody>());

        Container.Bind<CapsuleCollider>()
            .WithId("MainCollider")
            .FromInstance(GetComponent<CapsuleCollider>());
    }

    private void BindTigerMovement() {
        Container.Bind<IMovementController>()
            .To<TigerMovementController>()
            .FromInstance(GetComponent<TigerMovementController>());
    }

    private void BindStats() {
        Container.Bind<HealthHandler>()
            .FromInstance(GetComponent<HealthHandler>());
    }
}