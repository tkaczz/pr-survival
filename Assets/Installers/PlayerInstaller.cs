using System;
using UnityEngine;
using Zenject;
using Player;
using Pawn;

public class PlayerInstaller : MonoInstaller<PlayerInstaller> {
    [SerializeField] private Transform playerTransform;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private Camera playerCamera;

    public override void InstallBindings() {
        BindInput();
        BindPlayerController();
        BindPlayerPawn();
        BindRigidbodyController();
        BindPositions();
        BindPlayerMovement();
        BindPickup();
        BindStats();
    }

    private void BindStats() {
        Container.Bind<HealthHandler>()
            .FromInstance(GetComponent<HealthHandler>());
    }

    private void BindInput() {
        Container
            .Bind<IInput>()
            .To<KeyboardMouseInputHandler>()
            .FromInstance(GetComponent<KeyboardMouseInputHandler>());
    }

    private void BindPlayerController() {
        Container.Bind<IController>()
            .To<PlayerController>()
            .FromInstance(GetComponent<PlayerController>());

        Container.Bind<Transform>()
            .WithId(Transforms.Pawn)
            .FromInstance(playerTransform);

        Container.Bind<Transform>()
            .WithId(Transforms.PlayerCamera)
            .FromInstance(cameraTransform);
    }

    private void BindPlayerPawn() {
        Container.Bind<IPawn>()
            .To<PlayerPawn>()
            .FromInstance(GetComponent<PlayerPawn>());
    }

    private void BindRigidbodyController() {
        Container.Bind<RigidbodyCharacterController>()
            .FromInstance(GetComponent<RigidbodyCharacterController>());

        Container.Bind<Rigidbody>()
            .FromInstance(GetComponent<Rigidbody>());
    }

    private void BindPositions() {
        Container.Bind<StandingPosition>()
            .FromInstance(GetComponent<StandingPosition>());

        Container.Bind<CrouchPosition>()
            .FromInstance(GetComponent<CrouchPosition>());

        Container.Bind<PositionController>()
            .FromInstance(GetComponent<PositionController>());

        Container.Bind<ColliderSwitcher>()
            .FromInstance(GetComponent<ColliderSwitcher>());

        Container.Bind<CapsuleCollider>()
            .WithId("MainCollider")
            .FromInstance(GetComponent<CapsuleCollider>());
    }

    private void BindPlayerMovement() {
        Container.Bind<IMovementController>()
            .To<PlayerMovement>()
            .FromInstance(GetComponent<PlayerMovement>());
    }

    private void BindPickup() {
        Container.Bind<PickupHandler>()
            .FromInstance(GetComponent<PickupHandler>());
    }
}